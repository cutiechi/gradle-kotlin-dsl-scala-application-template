object Dependencies {

    /**
     * scala library
     */
    const val scalaLibrary = "org.scala-lang:scala-library:${Versions.scalaLibrary}"
}
