object Versions {

    /**
     * scala
     */
    private const val scala = "2.12.8"

    /**
     * scala library
     */
    const val scalaLibrary = scala
}
