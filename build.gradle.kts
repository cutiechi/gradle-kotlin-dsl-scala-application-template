plugins {
    
    scala
}

repositories {
    
    mavenCentral()
}

dependencies {
    
    implementation(Dependencies.scalaLibrary)
}
